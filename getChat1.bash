#!/bin/bash
# 
# File:   getChat1.bash
# Author: colin
# Created on 25-Feb-2019, 11:38:04 PM
#

#Gajim has many windows!:
gajimWindows=($(xdotool search --name "Gajim"))
#print length of array:
echo "Gajim windows found: ${#gajimWindows[@]} "
wind=(${gajimWindows[-1]})
echo "Last window PID: ${gajimWindows[-1]} "

#check if a Gajim window exists:
if [[ ${#gajimWindows[@]} == 0 ]]; then

#If it has no windows, Gajim must not be running- start it:
#Be sure your default window size for Gajim is set correctly!
(/usr/bin/gajim &)

else

xdotool windowactivate --sync $wind
#move over chat text:
xdotool mousemove --window $wind --sync 270 200
#click to remove any selection:
xdotool click --clearmodifiers 1
#sleep 1
#right click on chat text:
xdotool click --clearmodifiers 3
#sleep 1
#move mouse over "select all" option:
xdotool mousemove --window $wind --sync 300 550
#sleep 1
xdotool click --clearmodifiers 1
#sleep 1
xdotool key --clearmodifiers ctrl+c

fi
