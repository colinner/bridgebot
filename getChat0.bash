#!/bin/bash
# 
# File:   getChat0.bash
# Author: colin
# Created on 25-Feb-2019, 10:30:54 PM
#

#Messenger in firefox has a unique window and can therefore be searched:
xdotool search --name "Messenger" windowactivate --sync
#move mouse relative to the window:
xdotool search --name "Messenger" mousemove --window %1 1000 195
#send a click to the found window, click group title:
xdotool search --name "Messenger" click --repeat 2 --clearmodifiers 1
xdotool key --clearmodifiers ctrl+a
xdotool key --clearmodifiers ctrl+c