#!/bin/bash
# 
# File:   refreshChat0.bash
# Author: colin
# Created on 13-Apr-2019
#

#Messenger in firefox has a unique window and can therefore be searched:
xdotool search --name "Messenger" windowactivate --sync
#move mouse relative to the window, over Firefox's refresh button:
xdotool search --name "Messenger" mousemove --window %1 165 100
#send a click to the found window:
xdotool search --name "Messenger" click --clearmodifiers 1
sleep 8
