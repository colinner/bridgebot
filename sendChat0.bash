#!/bin/bash
# 
# File:   getChat0.bash
# Author: colin
# Created on 28-Feb-2019
#

#Messenger in firefox has a unique window and can therefore be searched:
xdotool search --name "Messenger" windowactivate --sync
#move mouse relative to the window over end of msg box:
xdotool search --name "Messenger" mousemove --window %1 1450 700
#click inside msg box:
xdotool search --name "Messenger" click --clearmodifiers 1
sleep 1
xdotool key --clearmodifiers ctrl+v
#wait for rich content to be fetched:
sleep 4
xdotool key --clearmodifiers Return
#press return again in case the message ended in a name, and the last enter just confirmed the name:
sleep 1
xdotool key --clearmodifiers Return
