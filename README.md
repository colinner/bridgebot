# bridgeBot

BridgeBot is a linux executable that acts as a bridge between any graphical chat clients, and also has some simple chatbot commands built-in.
The two clients that are supported by pre-supplied .bash files are for Facebook Messenger (Chat0) and Gajim xmpp (Chat1).
But in theory, these files could allow data exchange between any graphical clients, by telling xdotool how to copy/paste to/from the chat's GUI.
**If you decide to run your own instance,** you most likely will have to tweak the numbers in the .bash files to match the sizes of your windows.
Currently, the numbers are for a 3200x1800 screen with the windows in the upper left quarter of the screen.


The bot requires the following packages installed on your linux:
* xdotool
* xclip

These are really small and generally useful programs, I'd reccommed having them even without bridgeBot!

Future plans:
* adding a HTML parser to enable more detailed extraction from web clients like Facebook messenger
* adding recusive commands to enable turing-complete in-chat programming!

This is the master repository where I post all bridgeBot's source code. Feel free to write and submit commands, or improve the chat parser for more clients.