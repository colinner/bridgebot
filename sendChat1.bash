#!/bin/bash
# 
# File:   sendChat1.bash
# Author: colin
# Created on 28-Feb-2019
#

#Gajim has many windows!
#old way was to get window ID with xdotool getactivewindow after a delay:
gajimWindows=($(xdotool search --name "Gajim"))
wind=(${gajimWindows[-1]})
#assume Gajim is maintained open and ready by the getChat bash script.


xdotool windowactivate --sync $wind
#move over chat text:
xdotool mousemove --window $wind --sync 350 780
sleep 1
#click to remove any selection (maybe not required?):
xdotool click --clearmodifiers 1
#sleep 1
xdotool key --clearmodifiers ctrl+a
#sleep 1
xdotool key --clearmodifiers ctrl+v
sleep 1
xdotool key --clearmodifiers Return
