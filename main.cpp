/* 
 * BridgeBot Project
 * File:   main.cpp
 * Started Jan 27 by Colin Leitner
 * Ported to standard C++11 from GML on Feb 16 by Colin Leitner
 * This file calls 2 bash files per chat client: getChatX.bash and sendChatX.bash
 * These files must be in the working dir of the program, and be customized
 * for the screen of the computer its running on.
 */

#include <cstdlib>
#include <iostream> //for debug and status output to CLI
#include <string> //c++ string objects
#include <vector> //vectors (dynamically allocated arrays) for storing chatCache
#include <stdio.h> //for piping from shell

using namespace std;

//Bot Constants:
const double botVersion = 8.0;
long botStarted; //this holds the unix time of when the bot's process started for tracking uptime
long lastRefresh; //last time that a client refresh routine was run
const long refreshInterval = 86000; //seconds between refresh routine calls (minimum), 86400 sec/day
string triggerStr = "";

//Default Config:
int bridgedChats = 2; //n-way bridge! :D
int cycleDelay = 5; //seconds

//Chat Objects: //TODO: another way of doing this would be to have a struct or class per chatroom that contains each of these vars
vector<vector<string>> chatCache; //a vector of strings for each chatroom
vector<string> chatLastMsg; //string holding the last known message of a chat, used to detect new messages when reparsing
vector<bool> chatSynced;
vector<int> chatNewMsgs;
//Chat Constants:
vector<int> chatTrim;
vector<string> chatPrompt; //text on page immediately following last message; text will be discarded here on
vector<string> chatNonMsg; //any message that matches this will be excluded from cache (useful for gajim's horz line)

//TODO: would be good to store in a persistent file: chatLastMsg[] and bot's userMem (future)

//following function was inspired by user MeetTitan at https://stackoverflow.com/questions/32039852/returning-output-from-bash-script-to-calling-c-function
string execBash(char* cmd) {
	FILE* pipe = popen(cmd, "r");
	if (!pipe) {
		return "error opening pipe";
	}
	char buffer[16384]; //this is really big because it needs to hold all chars of the copied chatroom
	string cmdOutput = "";
	while (!feof(pipe)) { //TODO this loop may be intended to continue pulling text from the pipe in short strings, but it does not seem to work
		if (fgets(buffer, 16384, pipe) != NULL) {
			cmdOutput += buffer;
		}
	}
	pclose(pipe);
	return cmdOutput;
}

string cleanString(string str) {
	while (str.front() == ' ') {
		str.erase(0); //delete char 0 of the string
	}
	while ((str.back() == ' ') || (str.back() == '\n')) {
		str.erase(str.length()-1); //delete last char of the string
	}
	//escape all double quotes:
	int searched = 0;
	while (str.find('"', searched) != string::npos) { //while there is not nothing found
		searched = str.find('"', searched); //this could be more optimized, by not searching again
		str.insert(searched, "\\");
		searched += 2; //because a char was just added, and also need to skip to next char
	}
	return str;
}

//turns chatStr (delimited by newlines) into a vector of msgs
int parseChatByLine(unsigned int chatn, string chatStr) {
	int status = 0; //this will crudely accumulate any error values
	if (chatStr.length() > 0) {
		chatStr.erase(0, chatTrim[chatn]);
		chatStr.append("\n"); //Gajim's last msg not followed by \n
		
		bool atPrompt = false;
		string tempMsg = ""; //temporary string formed by delimiting newlines, candidate for a message, or could be junk shown by client
		int trimPos = 0; //remember where to trim to avoid calling find() twice
		while (!atPrompt && (chatStr.length() > 1)) { //1 is arbitrary, all chats have at least this many chars after last user message
			trimPos = chatStr.find("\n"); //determine where next newline is
			if (trimPos > -1) {
				tempMsg = chatStr.substr(0, trimPos); //copy the start of chatStr upto but not include the newline
				chatStr.erase(0, trimPos+1); //delete same portion of string as above, but also newline chars
				
				//begin analysis of tempMsg to determine if it is a real message:
				if ((tempMsg.length() > 0) && (tempMsg != chatNonMsg[chatn])) { //check for blank lines or other illegal messages
					if (!(chatPrompt[chatn].empty()) && (tempMsg.find(chatPrompt[chatn]) == 0)) {
						atPrompt = true;
					}
					else { //this is a valid message; therefore push it to the chatCache:
						chatCache[chatn].push_back(cleanString(tempMsg)); //trim spaces and escape quotes to block bash code injection!
					}
				}
			}
			else { //there are no newlines left to find
				cout << "Didn't reach chatPrompt!" << endl;
				atPrompt = true;
			}
		}
	}
	else { status += 1; } //can't work with an empty string
	
	return status;
}

//sends x11 commands via bash and xdotool to copy the given chatroom's content; returns exit value of system()
int getChat(unsigned int chatn) {
	int status = 0; //this will crudely accumulate any error values
	
	status += system(("./getChat" + to_string(chatn) + ".bash").c_str());
	
	//chatroom content is now in clipboard, now pipe it into a local string var:
	string chatContent = execBash("xclip -out -selection clipboard"); //TODO string constant doesn't comply with ISO C++ ?
	status += parseChatByLine(chatn, chatContent); //convert chatStr into array of messages
	
	return status;
}

void sendChat(string msg, int chatK) {
	if (msg.find(chatPrompt[chatK]) != 0) { //final check to prevent sending the chat prompt to it's own chat- doing so will block all following messages until it goes out of scope!!
		//following cmd is rather dangerous, and it is imperative that double quotes are escaped:
		system(("echo -n \"" + msg + "\" | xclip -i -selection clipboard").c_str());
		if (msg != "") { //prevent sending empty strings- causes sending a "like" on clients such as FB
			cout << "Sending to chat " << chatK << endl;
			system(("./sendChat" + to_string(chatK) + ".bash").c_str());
			system("sleep 2");
		}
		else { cout << "Tried to send empty string!" << endl; }
	}
	else { cout << "Tried to send chat prompt!" << endl; }
	chatSynced[chatK] = false; //flag this chat as unscanned and should not forward new content
	return;
}

string botCommand(string cmdStr, int trigLen) {
	string outStr = "";
	if (cmdStr == "help")			{ outStr = "Syntax: !cmd [args] Commands: !help !hello !time !datetime !shrug !trigger [value] !mem [name] [value] !recall [name]"; }
	else if (cmdStr == "hello")		{ outStr = "Hi, I'm a chatbot. Version: "+to_string(botVersion)+" Uptime: "+to_string((time(nullptr)-botStarted)/86400)+" days"; }
	else if (cmdStr == "datetime")	{ outStr = to_string(time(nullptr)); }
	else if (cmdStr == "shrug")		{ outStr = "¯\\_(ツ)_/¯"; } //this surprisingly doesn't break anything... yet
	else { if (trigLen > 0)			{ outStr = "Unknown command. Use !help"; } }
	return outStr;
}

int main(int argc, char** argv) {
	botStarted = time(nullptr); //gets current time
	lastRefresh = time(nullptr);
	cout << "BridgeBot started at unix time: " << botStarted << endl;
	
	//setup the requested number of chats and resize all vectors:
	//TODO this section will be easily replaced by a config file parser instead of hardcoding.
	//see object declarations at start of this source file for descriptions of chat parameters
	triggerStr = "!hello"; //TODO TEMP
	bridgedChats = 2;
	chatTrim.resize(bridgedChats);
	chatPrompt.resize(bridgedChats);
	chatNonMsg.resize(bridgedChats);
	
	chatTrim[0] = 0;
	chatPrompt[0] = "Type a message";
	chatNonMsg[0] = ""; //no known patterns to ignore
	
	chatTrim[1] = 0;
	chatPrompt[1] = "&"; //gajim has no text after last message, not even linebreak
	chatNonMsg[1] = "――――――――――――――――――――"; //gajim horz line
	
	//initialize chat vars:
	chatLastMsg.resize(bridgedChats, "");
	chatSynced.resize(bridgedChats, false);
	chatNewMsgs.resize(bridgedChats, 0);
	
while (true) { //TODO find a way to exit without killing?
	chatCache.clear();
	chatCache.resize(bridgedChats, vector<string>(0, "")); //ensure structure contains empty strings
	
	for (int chatN = 0; chatN < bridgedChats; chatN++) {
		int errOutput = getChat(chatN);
		cout << endl << "Chat " << chatN << ": getChat() returned: " << errOutput << endl;
		cout << chatCache[chatN].size() << " messages." << endl;
		
		if (chatCache[chatN].size() > 0) { //length should only ==0 if chat was not copied properly (usually OS lag)
			//check for new messages in chatN:
			if (!chatSynced[chatN]) { //initialize if chat has new messages, but ones that shouldn't be forwarded.
				chatNewMsgs[chatN] = 0;
				chatSynced[chatN] = true;
			}
			else { //find where the last known message appears in the chatCache[]:
				int i = chatCache[chatN].size() - 1; //i=index of chatCache[] start at last populated index and work backwards
				int iMin = i - 5; //TODO this could be user settable to increase number of lines that can be forwarded per cycle. This should be kept low to prevent a far back message being falsely identified as a new message
				if (iMin < 0) {
					iMin = 0;
					cout << "Chat has few msgs! iMin set to 0";
				}
				while ((chatCache[chatN][i] != chatLastMsg[chatN]) && (i > iMin)) { //go back only so many messages or until msg matches
					i -= 1;
				}
				if (i > iMin) { //lastMsg was found; record number of new msgs:
					chatNewMsgs[chatN] = (chatCache[chatN].size() - 1) - i; //if i unchanged, then result is 0=NewMsgs
				}
				else { //if exceeded max messages, do not assume entire chat is new. Just ignore remaining msgs :(
					chatNewMsgs[chatN] = 0;
					cout << "Can't find LastMsg! UI error?" << endl;
				}
				cout << "New msgs: " << chatNewMsgs[chatN] << endl;
			}
			chatLastMsg[chatN] = chatCache[chatN].back(); //update lastMsg
			cout << "Last msg: " << endl << chatLastMsg[chatN] <<endl;
		}
		else {
			cout << "ChatCache has no messages for this chat!" << endl;
		}
		system("sleep 1");
	}
	
	//next for loop must be separate because it will begin to output back to chats:
	for (int chatN = 0; chatN < bridgedChats; chatN++) {
		if ((chatNewMsgs[chatN] > 0) && (chatCache[chatN].size() > 0)) { //nothing to do with this chat if no new messages. Checking the chatLength[] prevents an AOOB error if the macro failed to copy from the chat
			cout << "Chat " << chatN << " has " << chatNewMsgs[chatN] << " new msgs." << endl;
			//one msg in chat at a time:
			int i = chatCache[chatN].size() - chatNewMsgs[chatN];
			while (i < chatCache[chatN].size()) {
				string parseTemp = chatCache[chatN][i];
				//check if this message starts with the triggerStr for commands:
				if ((parseTemp.find(triggerStr, 0) != -1)) { //if trigger is empty, all msgs are considered commands!
					//message is a command:
					string commandStr = parseTemp.substr(triggerStr.length(), string::npos); //remove trigger
					//TODO make argStr variable
					cout << "Command: " << commandStr <<endl;
					string outStr = "";
					outStr = botCommand(commandStr, triggerStr.length());
					cout << outStr << endl;
					outStr = "Uptime: "+to_string((double)(time(nullptr)-botStarted)/86400)+" days"; //TODO temporary!!
					sendChat(outStr, chatN); //TODO currently, only send response back to originating chat
				}
				else {
					//Send this text message to all other chats:
					for (int chatK = 0; chatK<bridgedChats; chatK += 1) { //TODO have option to broadcast cmds?
						if (chatK != chatN) { //don't send back to originating chat
							string textToSend = "";
							textToSend = chatCache[chatN][i];
							sendChat(textToSend, chatK); //TODO any point in having this middleman var?
						}
					}
				}
				i += 1;
			}
		}
	}
	
	if (lastRefresh + refreshInterval > time(nullptr)) { //have not reached the next checkpoint yet:
		cout << "Refreshing in " << to_string(lastRefresh + refreshInterval - time(nullptr)) << " sec" << endl;
	}
	else { //timer is up:
		cout << "Refreshing clients..." << endl;
		system(("./refreshChat" + to_string(0) + ".bash").c_str()); //TODO refresh ALL clients
		lastRefresh = time(nullptr); //reset timer
	}
	cout << "---------------" << endl;
	system(("sleep " + to_string(cycleDelay)).c_str()); //give opportunity to manually kill process
}
	
	return 0;
}
